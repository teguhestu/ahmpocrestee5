/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.fujitsu.id.sample.dao;

import com.fujitsu.id.common.dao.CrudDAO;
import com.fujitsu.id.sample.dto.EmployeeDTO;
import com.fujitsu.id.sample.dto.EmployeeSearchDTO;

/**
 *
 * @author teguhpa
 */
public interface EmployeeDAO extends CrudDAO<EmployeeDTO, EmployeeSearchDTO> {
   
}
