/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fujitsu.id.sample.dao;

import com.fujitsu.id.sample.dto.AuthorizationTokenDTO;

/**
 *
 * @author teguh.estu
 */
public interface UserDAO {
    public String getUserPassword(String user);
    public void createToken(AuthorizationTokenDTO dto);
    public AuthorizationTokenDTO getToken(String token);
    public void deleteToken(String user);
    //public int roleExist(String user, String role);
    //public int permissionExist(String user, String permission);
}
