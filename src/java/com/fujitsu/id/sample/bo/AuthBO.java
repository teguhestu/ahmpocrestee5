/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fujitsu.id.sample.bo;

import com.fujitsu.id.sample.dto.UserDTO;

/**
 *
 * @author teguh.estu
 */
public interface AuthBO {
    public String login(UserDTO dto);
    public boolean isAuthenticated(String token);
    public boolean isRoleAllowed(String token);
    public boolean isPermissionAllowed(String token, String permission);
    public void logout(String token);
}
