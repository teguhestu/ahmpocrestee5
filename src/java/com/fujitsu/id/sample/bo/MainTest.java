package com.fujitsu.id.sample.bo;


import com.fujitsu.id.common.dto.PagingResultDTO;
import com.fujitsu.id.sample.dto.EmployeeDTO;
import com.fujitsu.id.sample.dto.EmployeeSearchDTO;
import com.fujitsu.id.sample.dto.MasterGateDTO;
import com.fujitsu.id.sample.dto.MasterGateSearchDTO;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author teguhpa
 */
 @Component
public class MainTest {
    
    public static void main(String[] args) {
        ApplicationContext context = 
            new ClassPathXmlApplicationContext("applicationContext.xml");

        MainTest p =  context.getBean(MainTest.class);
        p.start(args);
    }

    @Autowired
    private MasterGateBO employeeService;
    
    
    /*public static void main(String[] args) {
        ApplicationContext context = 
             new ClassPathXmlApplicationContext("applicationContext.xml");
        EmployeeSearchDTO searchDTO = new EmployeeSearchDTO();
        searchDTO.setEmpNo(1001);
        EmployeeServiceImpl employeeService = new EmployeeServiceImpl();
        List<EmployeeDTO> employeeDTOs = employeeService.search(searchDTO);
        System.out.println(employeeDTOs.size());
        
    }*/

    private void start(String[] args) {
        EmployeeSearchDTO searchDTO = new EmployeeSearchDTO();
        try {
        employeeService.create(new MasterGateDTO());
        } catch (Exception e) {
            e.printStackTrace();;
             Set<ConstraintViolation<?>> errors = ((ConstraintViolationException) e).getConstraintViolations();
            Iterator<ConstraintViolation<?>> iterator = errors.iterator();
            while (iterator.hasNext()) {
                ConstraintViolation<?> element = iterator.next();
                System.out.println(element.getMessage());
            }
            return;
        }
        //searchDTO.setEmpNo(10001);
      //  EmployeeServiceImpl employeeService = new EmployeeServiceImpl();
      //  List<EmployeeDTO> employeeDTOs = employeeService.search(searchDTO);
        
      //  System.out.println(employeeDTOs.size());
            for (int i = 1; i < 11; i++) {
                System.out.println("Page: " + i +" ========================================================");
            PagingResultDTO<MasterGateDTO> dto = employeeService.searchByPage(new MasterGateSearchDTO(), i, 5*i);

            List<MasterGateDTO> dTOs = dto.getDto();
            System.out.println("Number of Page : "+ dto.getTotalPage());
            for (MasterGateDTO employeeDTO : dTOs) {
                //System.out.println(employeeDTO.getEmpNo() +" : " +employeeDTO.getFirstName());
        }
        }
        
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
