/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.fujitsu.id.sample.bo;

import com.fujitsu.id.common.dto.PagingResultDTO;
import com.fujitsu.id.sample.dto.EmployeeDTO;
import com.fujitsu.id.sample.dto.EmployeeSearchDTO;

/**
 *
 * @author teguhpa
 */
public interface EmployeeBO {
    public PagingResultDTO<EmployeeDTO> searchByPage(EmployeeSearchDTO searchDTO, int currentPage, int rowsPerPage);
    public void create(EmployeeDTO employeeDTO);
    public void update(EmployeeDTO employeeDTO);
    public void delete(EmployeeDTO employeeDTO);
}
