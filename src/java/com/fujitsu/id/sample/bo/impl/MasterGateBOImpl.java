/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.fujitsu.id.sample.bo.impl;

import com.fujitsu.id.common.bo.CrudBO;
import com.fujitsu.id.common.dao.CrudDAO;
import com.fujitsu.id.sample.bo.MasterGateBO;
import com.fujitsu.id.sample.dao.MasterGateDAO;
import com.fujitsu.id.sample.dto.MasterGateDTO;
import com.fujitsu.id.sample.dto.MasterGateSearchDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author teguhpa
 */
@Service
public class MasterGateBOImpl extends CrudBO<MasterGateDTO, MasterGateSearchDTO> implements MasterGateBO {

   @Autowired
   protected MasterGateDAO masterGateDAO;

    @Override
    public CrudDAO getDAO() {
        return masterGateDAO;
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}   
    
