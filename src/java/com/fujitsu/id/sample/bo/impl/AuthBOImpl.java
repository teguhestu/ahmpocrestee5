/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fujitsu.id.sample.bo.impl;

import com.fujitsu.id.sample.bo.AuthBO;
import com.fujitsu.id.common.exception.LoginFailureException;
import com.fujitsu.id.common.exception.NotAuthorizedException;
import com.fujitsu.id.sample.dto.AuthorizationTokenDTO;
import com.fujitsu.id.sample.dto.UserDTO;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.fujitsu.id.sample.hsql.dao.UserHsqlDAO;

/**
 *
 * @author teguh.estu
 */
@Service
public class AuthBOImpl implements AuthBO {

    @Autowired
    private UserHsqlDAO userHsqlDAO;
    
    @Transactional
    public String login(UserDTO dto) {
        AuthorizationTokenDTO token = new AuthorizationTokenDTO();
        String password = userHsqlDAO.getUserPassword(dto.getUser());
        if (dto.getPassword().equals(password)) {
             token = new AuthorizationTokenDTO(dto.getUser());
             userHsqlDAO.createToken(token);
        } //else {
        //    throw new LoginFailureException("Login Failed");
       // }
        
        return token != null? token.getToken():"";
    }
    
    @Transactional
    public void logout(String token) {
        userHsqlDAO.deleteToken(token);
    }

    public boolean isAuthenticated(String token) {
        Date dt = new Date();
        AuthorizationTokenDTO tokenDTO = userHsqlDAO.getToken(token);
        if (tokenDTO == null) {
            throw new NotAuthorizedException("Token does not exist or already expired");
        }
        else if (tokenDTO.getExpiryDate().before(dt)) {
            throw new NotAuthorizedException("Token does not exist or already expired");
        }
        return true;
    }

    public boolean isRoleAllowed(String token) {
        return true;
    }

    public boolean isPermissionAllowed(String token, String permission) {
        if (userHsqlDAO.permissionAllowed(token, permission) > 0) {
            return true;
        } else {
            return false;
        }
        
    }

    
}
