/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.fujitsu.id.sample.bo.impl;

import com.fujitsu.id.common.bo.CrudBO;
import com.fujitsu.id.common.dao.CrudDAO;
import com.fujitsu.id.sample.bo.EmployeeBO;
import com.fujitsu.id.sample.dao.EmployeeDAO;
import com.fujitsu.id.sample.dto.EmployeeDTO;
import com.fujitsu.id.sample.dto.EmployeeSearchDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author teguhpa
 */
@Service
public class EmployeeBOImpl extends CrudBO<EmployeeDTO, EmployeeSearchDTO> implements EmployeeBO {

   @Autowired
   protected EmployeeDAO employeeDAO;

    @Override
    public CrudDAO getDAO() {
        return employeeDAO;
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}   
    
