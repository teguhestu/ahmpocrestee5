/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fujitsu.id.sample.bo;

import com.fujitsu.id.common.dto.PagingResultDTO;
import com.fujitsu.id.sample.dto.MasterGateDTO;
import com.fujitsu.id.sample.dto.MasterGateSearchDTO;
import java.util.List;

/**
 *
 * @author teguh.estu
 */
public interface MasterGateBO {
    public PagingResultDTO<MasterGateDTO> searchByPage(MasterGateSearchDTO searchDTO, int currentPage, int rowsPerPage);
    public void create(MasterGateDTO employeeDTO);
    public void update(MasterGateDTO employeeDTO);
    public void delete(MasterGateDTO employeeDTO);
    public List<MasterGateDTO> search(MasterGateSearchDTO searchDTO);
}
