/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fujitsu.id.sample.hsql.dao;

import com.fujitsu.id.sample.dto.AuthorizationTokenDTO;
import org.apache.ibatis.annotations.Param;

/**
 *
 * @author teguh.estu
 */
public interface UserHsqlDAO {
    public String getUserPassword(String user);
    public void createToken(AuthorizationTokenDTO dto);
    public AuthorizationTokenDTO getToken(String token);
    public void deleteToken(String user);
    //public int roleExist(String user, String role);
    public int permissionAllowed(@Param("token") String token, 
            @Param("permission") String permission);
}
