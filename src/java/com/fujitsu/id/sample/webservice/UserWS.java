/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fujitsu.id.sample.webservice;

import com.fujitsu.id.sample.bo.AuthBO;
import com.fujitsu.id.common.exception.LoginFailureException;
import com.fujitsu.id.sample.dto.UserDTO;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author teguh.estu
 */
@Component
@Path("/user")
public class UserWS {
    
    @Autowired
    private AuthBO authBO;
    
    @Path("login")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @POST
    public String login(UserDTO dto) {
       
        try {
            String status = "error";
            
            String token = authBO.login(dto);
            if (token != null && !"".equals(token)) {
                 status = "success";
            }
            return "{\"status\": \""+status+"\", \"token\": \""+token+"\"}";
        } catch (LoginFailureException e) {
            e.printStackTrace();
            return Response.status(Response.Status.UNAUTHORIZED).toString();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).toString();
        }
                    
    }
    
}
