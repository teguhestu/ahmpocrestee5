/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fujitsu.id.sample.webservice;

import com.fujitsu.id.common.dto.JSONResult;
import com.fujitsu.id.common.dto.JSONResultDTO;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.ws.rs.WebApplicationException;
import org.apache.ibatis.exceptions.PersistenceException;

/**
 *
 * @author teguhpa
 */
public class BaseWS {

    protected JSONResultDTO processException(Exception e) {
        JSONResultDTO result = new JSONResultDTO();
        if (e instanceof ConstraintViolationException) {
            
            e.printStackTrace();
            List<String> message = new ArrayList<String>();
            Set<ConstraintViolation<?>> errors = ((ConstraintViolationException) e).getConstraintViolations();
            Iterator<ConstraintViolation<?>> iterator = errors.iterator();
            while (iterator.hasNext()) {
                ConstraintViolation<?> element = iterator.next();
                message.add(element.getMessage());
            }
            result.setReturnValue(1);
            result.setMessages(message);
        } else if (e instanceof PersistenceException) {
            e.printStackTrace();
            List<String> message = new ArrayList<String>();
            message.add(e.getMessage());
            result.setReturnValue(2);
            result.setMessages(message);
        } else if (e instanceof WebApplicationException) {
            e.printStackTrace();
            List<String> message = new ArrayList<String>();
            message.add(e.getMessage());
            result.setReturnValue(2);
            result.setMessages(message);
        } else {
            e.printStackTrace();
            List<String> message = new ArrayList<String>();
            message.add(e.getMessage());
            result.setReturnValue(2);
            result.setMessages(message);
        }
        return result;
    }
}
