/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fujitsu.id.sample.webservice;

import com.fujitsu.id.common.exception.FIDExceptionMapper;
import java.util.HashSet;
import java.util.Set;
import javax.ws.rs.core.Application;

/**
 *
 * @author teguh.estu
 */
public class RESTConfig extends Application {
    @Override
    public Set<Object> getSingletons() {
        Set<Object> singletons = new HashSet<Object>();
       // singletons.add(new SubjectInjectableProvider());
        singletons.add(new FIDExceptionMapper());
        return singletons;
    }
    
}
