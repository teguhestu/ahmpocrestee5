/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fujitsu.id.sample.webservice;

import com.fujitsu.id.common.auth.RequiresPermissions;
import com.fujitsu.id.common.auth.RequiresUser;
import com.fujitsu.id.common.dto.JSONResultDTO;
import com.fujitsu.id.sample.bo.MasterGateBO;
import com.fujitsu.id.sample.dto.MasterGateDTO;
import com.fujitsu.id.sample.dto.MasterGateSearchDTO;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author teguhpa
 */
@Component
@Path("/mastergate")
public class MasterGateWS extends BaseWS {

    @Autowired
    private MasterGateBO masterGateBO;

    @GET
    @Path("test")
    public String test () {
        return "abc";
    }
    
    @POST
    @Path("query")
   @RequiresUser
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public JSONResultDTO getMasterGates(MasterGateSearchDTO searchDto) {
         JSONResultDTO result = new JSONResultDTO();
        
        try {
            List<MasterGateDTO> dto = masterGateBO.search(searchDto);
            List<String> message = new ArrayList<String>();
            message.add("Data retrieved Sucessfully");
            result.setResult(dto);
            result.setReturnValue(0);
            result.setMessages(message);
        }  catch (Exception e) {
             result = processException(e);
             //return Response.status(Response.Status.NOT_FOUND).entity(new JSONResult(result).getJSONString()).build();
        }

        return result;
        //return Response.ok().entity(result).build();
    }
    
    @GET
    @RequiresUser
    @Path("query")
    @Produces({MediaType.APPLICATION_JSON})
    public JSONResultDTO getMasterGates(@QueryParam("mstorMplantVplantid") String plantId,
            @QueryParam("vgateid") String gateId,
            @QueryParam("mstorVslocid") String locId) {
         JSONResultDTO result = new JSONResultDTO();
         MasterGateSearchDTO searchDto = new MasterGateSearchDTO();
         searchDto.setVgateid("".equals(gateId)? null : gateId);
         searchDto.setMstorMplantVplantid("".equals(plantId)? null : plantId);
         searchDto.setMstorVslocid("".equals(locId)? null : locId);
        return getMasterGates(searchDto);
        //return Response.ok().entity(result).build();
    }
    
    @GET
    @RequiresUser
    @Produces({MediaType.APPLICATION_JSON})
    public JSONResultDTO getMasterGates() {
        return getMasterGates(new MasterGateSearchDTO());
    }
    
    
    @GET
    @RequiresUser
    @Path("{id}")
    @Produces({MediaType.APPLICATION_JSON})
    public JSONResultDTO getMasterGates(@PathParam("id") String id) {
        MasterGateSearchDTO dto = new MasterGateSearchDTO();
        dto.setVgateid(id);
        return getMasterGates(dto);
    }

    @POST
    @RequiresUser
    @RequiresPermissions(value = {"mastergate_add"})
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public JSONResultDTO create(MasterGateDTO dto) {
        JSONResultDTO result = new JSONResultDTO();
        try {
            //MasterGateDTO dto = param.getDTO(MasterGateDTO.class);
            masterGateBO.create(dto);
            List<String> message = new ArrayList<String>();
            message.add("Data Inserted Sucessfully");
           
            result.setResult(dto);
            result.setReturnValue(0);
            result.setMessages(message);
            
            //return Response.ok().entity(new JSONResult(result).getJSONString()).build();
            return result;
        } catch (Exception e) {
             result = processException(e);
             return result;
             //return Response.status(Response.Status.NOT_FOUND).entity(new JSONResult(result).getJSONString()).build();
        }
    }

    @PUT
    @RequiresUser
    @RequiresPermissions(value = {"mastergate_update"})
    @Path("{id}")
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public JSONResultDTO update(@PathParam("id") String id, MasterGateDTO dto) {
        JSONResultDTO result = new JSONResultDTO();
        try {
            //MasterGateDTO dto = param.getDTO(MasterGateDTO.class);
            dto.setVgateid(id);
            masterGateBO.update(dto);
            List<String> message = new ArrayList<String>();
            message.add("Data Updated Sucessfully");
           
            result.setResult(dto);
            result.setReturnValue(0);
            result.setMessages(message);
            
            //return Response.ok().entity(new JSONResult(result).getJSONString()).build();
            return result;
        } catch (Exception e) {
            result = processException(e);
            return result;
             //return Response.status(Response.Status.NOT_FOUND).entity(new JSONResult(result).getJSONString()).build();
        }
    }

    @DELETE
    @RequiresUser
    @RequiresPermissions(value = {"mastergate_delete"})
    @Path("{id}")
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public JSONResultDTO delete(@PathParam("id") String id) {
        JSONResultDTO result = new JSONResultDTO();
        try {
            MasterGateDTO dto = new MasterGateDTO();
            dto.setVgateid(id);
            //MasterGateDTO dto = param.getDTO(MasterGateDTO.class);
            masterGateBO.delete(dto);
            List<String> message = new ArrayList<String>();
            message.add("Data Deleted Sucessfully");
           
            //result.setResult(dto);
            result.setReturnValue(0);
            result.setMessages(message);
            return result;
            //return Response.ok().entity(new JSONResult(result).getJSONString()).build();
        } catch (Exception e) {
            result = processException(e);
            return result;
             //return Response.status(Response.Status.NOT_FOUND).entity(new JSONResult(result).getJSONString()).build();
        }
    }
}
