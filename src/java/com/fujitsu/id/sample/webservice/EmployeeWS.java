/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fujitsu.id.sample.webservice;

import com.fujitsu.id.common.dto.JSONParam;
import com.fujitsu.id.common.dto.JSONResult;
import com.fujitsu.id.common.dto.JSONResultDTO;
import com.fujitsu.id.common.dto.PagingResultDTO;
import com.fujitsu.id.sample.bo.EmployeeBO;
import com.fujitsu.id.sample.dto.EmployeeDTO;
import com.fujitsu.id.sample.dto.EmployeeSearchDTO;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author teguhpa
 */
@Component
@Path("/employee")
public class EmployeeWS extends BaseWS {

    @Autowired
    private EmployeeBO employeeBO;

    @GET
    @Path("test")
    public String test () {
        return "abc";
    }
    
    @POST
    @Path("search")
    @Produces({MediaType.APPLICATION_JSON})

    public Response getEmployee(@QueryParam("param") JSONParam<EmployeeSearchDTO> param) {
        String result = "";
        try {
            EmployeeSearchDTO dto = param.getDTO(EmployeeSearchDTO.class);
            PagingResultDTO<EmployeeDTO> resultDTO = employeeBO.searchByPage(dto, dto.getCurrentPage(), dto.getRowsPerPage());

            JSONResult jSONResult = new JSONResult(resultDTO);
            result = jSONResult.getJSONString();
        }  catch (Exception e) {
           // result = processException(e);
             return Response.status(Response.Status.NOT_FOUND).entity(new JSONResult(result).getJSONString()).build();
        }

        return Response.ok().entity(result).build();
    }

    @POST
    @Path("create")
    @Produces({MediaType.APPLICATION_JSON})
    public Response create(@QueryParam("param") JSONParam<EmployeeDTO> param) {
        JSONResultDTO result = new JSONResultDTO();
        try {
            EmployeeDTO dto = param.getDTO(EmployeeDTO.class);
            employeeBO.create(dto);
            List<String> message = new ArrayList<String>();
            message.add("Data Inserted Sucessfully");
           
            result.setResult(dto);
            result.setReturnValue(0);
            result.setMessages(message);
            
            return Response.ok().entity(new JSONResult(result).getJSONString()).build();
        } catch (Exception e) {
            result = processException(e);
             return Response.status(Response.Status.NOT_FOUND).entity(new JSONResult(result).getJSONString()).build();
        }
    }

    @POST
    @Path("update")
    public Response update(@QueryParam("param") JSONParam<EmployeeDTO> param) {
        EmployeeDTO dto = param.getDTO(EmployeeDTO.class);
        employeeBO.update(dto);
        return Response.ok().build();
    }

    @POST
    @Path("delete")
    public Response delete(@QueryParam("param") JSONParam<EmployeeDTO> param) {
        EmployeeDTO dto = param.getDTO(EmployeeDTO.class);
        employeeBO.delete(dto);
        return Response.ok().build();
    }
}
