/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fujitsu.id.common.auth;

/**
 *
 * @author teguh.estu
 */
public enum Logical {

    AND, OR;

    private Logical() {
        
    }
}

