/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fujitsu.id.common.validation;

import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.concurrent.ConcurrentMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.validation.MessageInterpolator;
import org.hibernate.validator.internal.engine.messageinterpolation.InterpolationTerm;
import org.hibernate.validator.internal.engine.messageinterpolation.LocalizedMessage;
import org.hibernate.validator.internal.util.CollectionHelper;
import org.hibernate.validator.resourceloading.PlatformResourceBundleLocator;
import org.hibernate.validator.spi.resourceloading.ResourceBundleLocator;

/**
 *
 * @author teguhpa
 */
public class CustomInterpolator
        implements MessageInterpolator {

    private static final String DEFAULT_VALIDATION_MESSAGES = "org.hibernate.validator.ValidationMessages";
    public static final String USER_VALIDATION_MESSAGES = "ValidationMessages";
    private static final Pattern MESSAGE_PARAMETER_PATTERN = Pattern.compile("((\\\\*)\\{[^\\}]+?\\})");

    private static final Pattern MESSAGE_EXPRESSION_PATTERN = Pattern.compile("((\\\\*)\\$?\\{[^\\}]+?\\})");
    private final Locale defaultLocale;
    private final ResourceBundleLocator userResourceBundleLocator;
    private final ResourceBundleLocator defaultResourceBundleLocator;
    private final ConcurrentMap<LocalizedMessage, String> resolvedMessages = CollectionHelper.newConcurrentHashMap();
    private final boolean cacheMessages;

    public CustomInterpolator() {
        this(null);
    }

    public CustomInterpolator(ResourceBundleLocator userResourceBundleLocator) {
        this(userResourceBundleLocator, true);
    }

    public CustomInterpolator(ResourceBundleLocator userResourceBundleLocator, boolean cacheMessages) {
        this.defaultLocale = Locale.getDefault();

        if (userResourceBundleLocator == null) {
            this.userResourceBundleLocator = new PlatformResourceBundleLocator("ValidationMessages");
        } else {
            this.userResourceBundleLocator = userResourceBundleLocator;
        }

        this.defaultResourceBundleLocator = new PlatformResourceBundleLocator("org.hibernate.validator.ValidationMessages");
        this.cacheMessages = cacheMessages;
    }

    @Override
    public String interpolate(String message, MessageInterpolator.Context context) {
        return interpolateMessage(message, context, this.defaultLocale);
    }

    @Override
    public String interpolate(String message, MessageInterpolator.Context context, Locale locale) {
        return interpolateMessage(message, context, locale);
    }

    private String interpolateMessage(String message, MessageInterpolator.Context context, Locale locale) {
        LocalizedMessage localisedMessage = new LocalizedMessage(message, locale);
        String resolvedMessage = null;

        if (this.cacheMessages) {
            resolvedMessage = (String) this.resolvedMessages.get(localisedMessage);
        }

        if (resolvedMessage == null) {
            ResourceBundle userResourceBundle = this.userResourceBundleLocator.getResourceBundle(locale);

            ResourceBundle defaultResourceBundle = this.defaultResourceBundleLocator.getResourceBundle(locale);

            resolvedMessage = message;
            boolean evaluatedDefaultBundleOnce = false;
            while (true) {
                String userBundleResolvedMessage = interpolateBundleMessage(resolvedMessage, userResourceBundle, locale, true);

                if ((evaluatedDefaultBundleOnce) && (!hasReplacementTakenPlace(userBundleResolvedMessage, resolvedMessage))) {
                    break;
                }

                resolvedMessage = interpolateBundleMessage(userBundleResolvedMessage, defaultResourceBundle, locale, false);

                evaluatedDefaultBundleOnce = true;
            }

        }

        if (this.cacheMessages) {
            String cachedResolvedMessage = (String) this.resolvedMessages.putIfAbsent(localisedMessage, resolvedMessage);
            if (cachedResolvedMessage != null) {
                resolvedMessage = cachedResolvedMessage;
            }

        }

        resolvedMessage = interpolateExpression(resolvedMessage, MESSAGE_PARAMETER_PATTERN, context, locale);

        resolvedMessage = interpolateExpression(resolvedMessage, MESSAGE_EXPRESSION_PATTERN, context, locale);

        resolvedMessage = resolvedMessage.replace("\\{", "{");
        resolvedMessage = resolvedMessage.replace("\\}", "}");
        resolvedMessage = resolvedMessage.replace("\\\\", "\\");
        resolvedMessage = resolvedMessage.replace("\\$", "$");

        return resolvedMessage;
    }

    private boolean hasReplacementTakenPlace(String origMessage, String newMessage) {
        return !origMessage.equals(newMessage);
    }

    private String interpolateBundleMessage(String message, ResourceBundle bundle, Locale locale, boolean recurse) {
        Matcher matcher = MESSAGE_PARAMETER_PATTERN.matcher(message);
        StringBuffer sb = new StringBuffer();

        while (matcher.find()) {
            String parameter = matcher.group(1);
            String resolvedParameterValue = resolveParameter(parameter, bundle, locale, recurse);

            matcher.appendReplacement(sb, Matcher.quoteReplacement(resolvedParameterValue));
        }
        matcher.appendTail(sb);
        return sb.toString();
    }

    private String interpolateExpression(String message, Pattern pattern, MessageInterpolator.Context context, Locale locale) {
        Matcher matcher = pattern.matcher(message);
        StringBuffer sb = new StringBuffer();

        while (matcher.find()) {
            String match = matcher.group(1);
            InterpolationTerm expression = new InterpolationTerm(match, locale);
            if (expression.needsEvaluation()) {
                String resolvedExpression = expression.interpolate(context);
                resolvedExpression = Matcher.quoteReplacement(resolvedExpression);
                matcher.appendReplacement(sb, resolvedExpression);
            }
        }
        matcher.appendTail(sb);
        return sb.toString();
    }

    private String resolveParameter(String parameterName, ResourceBundle bundle, Locale locale, boolean recurse) {
        String parameterValue;
        try {
            if (bundle != null) {
                parameterValue = bundle.getString(removeCurlyBraces(parameterName));
                if (recurse) {
                    parameterValue = interpolateBundleMessage(parameterValue, bundle, locale, recurse);
                }
            } else {
                parameterValue = parameterName;
            }
        } catch (MissingResourceException e) {
            parameterValue = parameterName;
        }
        return parameterValue;
    }

    private String removeCurlyBraces(String parameter) {
        return parameter.substring(1, parameter.length() - 1);
    }
}
