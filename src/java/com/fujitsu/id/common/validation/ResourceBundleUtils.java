/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.fujitsu.id.common.validation;


import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author teguhpa
 */
public class ResourceBundleUtils {

    public static final Locale en_EN = new Locale("en", "EN");
    private static final Pattern PATTERN_FIND_NUMBER = Pattern.compile("\\{[0-9]\\}");

  
    public static String get(String key, String bundle, Object... array) {
        return get(key, bundle, null, array);
    }

    public static String get(String key, String bundle, ClassLoader classLoader, Object... array) {

        if(key == null || key.isEmpty()){
            return "ResourceBundle key is required";
        }
       
        Locale locale = Locale.getDefault();

        ResourceBundle resourceBundle = null;

        try {
            if (classLoader != null) {
                resourceBundle = ResourceBundle.getBundle(bundle, locale, classLoader);
            } else {
                resourceBundle = ResourceBundle.getBundle(bundle, locale);
            }

            if (resourceBundle == null && !locale.equals(en_EN)) {
                resourceBundle = ResourceBundle.getBundle(bundle, en_EN, classLoader);
            }

            if (resourceBundle == null || !resourceBundle.containsKey(key)) {
                return key;
            }

            key = resourceBundle.getString(key);

            if (array != null && array.length > 0) {
                Matcher matcher = PATTERN_FIND_NUMBER.matcher(key);
                while (matcher.find()) {
                    String chave = matcher.group();
                    int position = Integer.valueOf(getOnlyIntegerNumbers(chave));
                    if (position < array.length && array[position] != null) {
                        key = key.replace(chave, array[position].toString());
                    }
                }
                return key;
            }

        } catch (MissingResourceException ex2) {
            return key;
        }

        return key;
    }
    
    public static String getOnlyIntegerNumbers(String htmlString) {
        return htmlString.replaceAll("([^0-9])", "");
    }


}

