/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.fujitsu.id.common.validation;

import javax.validation.Validation;
import javax.validation.Validator;
import org.hibernate.validator.HibernateValidator;
import org.hibernate.validator.messageinterpolation.ResourceBundleMessageInterpolator;
import org.hibernate.validator.resourceloading.PlatformResourceBundleLocator;

/**
 *
 * @author teguhpa
 */
public class ValidatorUtil {
    public static Validator buildValidatorFactory() {
        Validator validator =
    Validation.byProvider(HibernateValidator.class)
        .configure()
        .messageInterpolator(
            new ResourceBundleMessageInterpolator(
                new PlatformResourceBundleLocator( "ApplicationResources" )))
        .buildValidatorFactory()
        .getValidator();
        
        return validator;
    }
}
