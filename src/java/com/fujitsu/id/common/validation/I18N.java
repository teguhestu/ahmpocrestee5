/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.fujitsu.id.common.validation;

/**
 *
 * @author teguhpa
 */
public class I18N {
     private static final String MESSAGE_BUNDLE = "ApplicationResources";
     
     public static String get(String key, Object... parameters) {
        return ResourceBundleUtils.get(key, MESSAGE_BUNDLE, Thread.currentThread().getContextClassLoader(), parameters);
    }

}
