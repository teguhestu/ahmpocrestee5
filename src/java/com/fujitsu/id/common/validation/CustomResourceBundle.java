/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.fujitsu.id.common.validation;

import java.util.Enumeration;
import java.util.ResourceBundle;

/**
 *
 * @author teguhpa
 */
public class CustomResourceBundle extends ResourceBundle{

    @Override
    public Enumeration<String> getKeys() {
        return parent.getKeys();
    }

    @Override
    protected Object handleGetObject(String key) {
        return I18N.get(key);
    }
    
}
