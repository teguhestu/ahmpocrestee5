/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fujitsu.id.common.filter;

import com.fujitsu.id.common.auth.RequiresPermissions;
import com.fujitsu.id.common.auth.RequiresUser;
import com.sun.jersey.api.model.AbstractMethod;
import com.sun.jersey.spi.container.ResourceFilter;
import com.sun.jersey.spi.container.ResourceFilterFactory;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import javax.ws.rs.ext.Provider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author teguh.estu
 */
@Component
@Provider
public class SecureRestFilterFactory implements ResourceFilterFactory {
    
    @Autowired
    private UserResourceFilter userResourceFilter;
    
    @Autowired
    private PermissionResourceFilter permissionResourceFilter;
    
   // @Autowired
   // PermissionResourceFilter permissionResourceFilter;

    @SuppressWarnings("unchecked")
    private static List<Class<? extends Annotation>> secureAnnotation = Collections.unmodifiableList(Arrays.asList(
            RequiresPermissions.class,
            RequiresUser.class/*,
            RequiresGuest.class*/));

    @Override
    public List<ResourceFilter> create(AbstractMethod am) {
        List<ResourceFilter> filters = new ArrayList<ResourceFilter>();
        
        for (Class<? extends Annotation> annotationClass : secureAnnotation) {
            // XXX What is the performance of getAnnotation vs getAnnotations?
            //Annotation classAuthzSpec = am.getResource().getAnnotation(annotationClass);
            Annotation methodAuthzSpec = am.getAnnotation(annotationClass);
            if (methodAuthzSpec != null) {
                Class<?> t = methodAuthzSpec.annotationType();
                if (RequiresUser.class.equals(t)) {
                    filters.add(userResourceFilter);
                }
                if (RequiresPermissions.class.equals(t)) {
                    permissionResourceFilter.setAnnotation(methodAuthzSpec);
                    filters.add(permissionResourceFilter);
                }
            }

           // if (classAuthzSpec != null) filters.add(SecureRestResourceFilter.valueOf(classAuthzSpec));
            //if (methodAuthzSpec != null) filters.add(SecureRestResourceFilter.valueOf(methodAuthzSpec));
        }

        return filters;
    }
    
}
