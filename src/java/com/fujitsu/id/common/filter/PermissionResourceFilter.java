/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fujitsu.id.common.filter;

import com.fujitsu.id.common.bo.PermissionsAnnotationHandler;
import com.fujitsu.id.common.exception.NotAuthorizedException;
import com.sun.jersey.spi.container.ContainerRequest;
import com.sun.jersey.spi.container.ContainerRequestFilter;
import com.sun.jersey.spi.container.ContainerResponseFilter;
import com.sun.jersey.spi.container.ResourceFilter;
import java.lang.annotation.Annotation;
import java.security.Principal;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.ext.Provider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author teguh.estu
 */
@Component
@Provider
public class PermissionResourceFilter implements ResourceFilter, ContainerRequestFilter {
    
    @Autowired
    private PermissionsAnnotationHandler permissionsAnnotationHandler;
    
    private Annotation annotation;
    
    public ContainerRequestFilter getRequestFilter() {
        return this;
    }

    public ContainerResponseFilter getResponseFilter() {
        return null;
    }

    public ContainerRequest filter(ContainerRequest requestContext) {
        String authorizationHeader
                = requestContext.getHeaderValue(HttpHeaders.AUTHORIZATION);

        // Check if the HTTP Authorization header is present and formatted correctly 
        if (authorizationHeader == null) {
            throw new WebApplicationException(Response.Status.UNAUTHORIZED);
        }

        // Extract the token from the HTTP Authorization header
        String token = authorizationHeader;
        final String username = "";
       
        try {

            requestContext.setSecurityContext(new SecurityContext() {

                @Override
                public Principal getUserPrincipal() {

                    return new Principal() {

                        @Override
                        public String getName() {
                            return username;
                        }
                    };
                }

                @Override
                public boolean isUserInRole(String role) {
                    return true;
                }

                @Override
                public boolean isSecure() {
                    return false;
                }

                @Override
                public String getAuthenticationScheme() {
                    return null;
                }
            });
        } catch (NotAuthorizedException e) {
            throw new WebApplicationException(Response.Status.UNAUTHORIZED);
        } finally {

        }
        
        permissionsAnnotationHandler.assertAuthorized(token, annotation);
        return requestContext;
    }

    void setAnnotation(Annotation methodAuthzSpec) {
        this.annotation = methodAuthzSpec;
    }
    
}
