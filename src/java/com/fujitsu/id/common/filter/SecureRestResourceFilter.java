/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fujitsu.id.common.filter;

import com.fujitsu.id.common.auth.RequiresPermissions;
import com.fujitsu.id.common.auth.RequiresUser;
import com.fujitsu.id.common.bo.AuthAnnotationHandler;
import com.fujitsu.id.common.bo.PermissionsAnnotationHandler;
import com.fujitsu.id.common.bo.UsersAnnotationHandler;
import com.fujitsu.id.common.exception.NotAuthorizedException;
import com.sun.jersey.spi.container.ContainerRequest;
import com.sun.jersey.spi.container.ContainerRequestFilter;
import com.sun.jersey.spi.container.ContainerResponseFilter;
import com.sun.jersey.spi.container.ResourceFilter;
import java.lang.annotation.Annotation;
import java.security.Principal;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.ext.Provider;
import org.springframework.stereotype.Component;

/**
 *
 * @author teguh.estu
 */

public class SecureRestResourceFilter<T extends Annotation> implements ResourceFilter, ContainerRequestFilter {

    private final T authzSpec;
    private final AuthAnnotationHandler handler;

    
    public SecureRestResourceFilter(T authzSpec, AuthAnnotationHandler handler) {
        this.authzSpec = authzSpec;
        this.handler = handler;
    }

    public static <T extends Annotation> SecureRestResourceFilter<T> valueOf(T authzSpec) {
        return new SecureRestResourceFilter<T>(authzSpec, defaultHandler(authzSpec));
    }

    private static AuthAnnotationHandler defaultHandler(Annotation annotation) {
        Class<?> t = annotation.annotationType();
        if (RequiresPermissions.class.equals(t)) {
            return new PermissionsAnnotationHandler();
        } //else if (RequiresRoles.class.equals(t)) return new RoleAnnotationHandler();
        else if (RequiresUser.class.equals(t)) {
            return new UsersAnnotationHandler();
        } //else if (RequiresGuest.class.equals(t)) return new GuestAnnotationHandler();
        //else if (RequiresAuthentication.class.equals(t)) return new AuthenticatedAnnotationHandler();
        else {
            throw new IllegalArgumentException("No default handler known for annotation " + t);
        }
    }

    public ContainerRequestFilter getRequestFilter() {
        return this;
    }

    public ContainerResponseFilter getResponseFilter() {
        return null;
    }

    public ContainerRequest filter(ContainerRequest requestContext) {

        String authorizationHeader
                = requestContext.getHeaderValue(HttpHeaders.AUTHORIZATION);

        // Check if the HTTP Authorization header is present and formatted correctly 
        if (authorizationHeader == null) {
            throw new WebApplicationException(Status.UNAUTHORIZED);
        }

        // Extract the token from the HTTP Authorization header
        String token = authorizationHeader;
        final String username = "";
       
        try {

            requestContext.setSecurityContext(new SecurityContext() {

                @Override
                public Principal getUserPrincipal() {

                    return new Principal() {

                        @Override
                        public String getName() {
                            return username;
                        }
                    };
                }

                @Override
                public boolean isUserInRole(String role) {
                    return true;
                }

                @Override
                public boolean isSecure() {
                    return false;
                }

                @Override
                public String getAuthenticationScheme() {
                    return null;
                }
            });
        } catch (NotAuthorizedException e) {
            throw new WebApplicationException(Status.UNAUTHORIZED);
        } finally {

        }
        
        handler.assertAuthorized(authzSpec, token);
        return requestContext;
    }

}
