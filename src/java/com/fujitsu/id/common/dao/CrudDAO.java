/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.fujitsu.id.common.dao;

import com.fujitsu.id.common.dto.EntityDTO;
import com.fujitsu.id.common.dto.PagingResultDTO;
import com.fujitsu.id.common.dto.SearchDTO;
import java.util.List;
import org.apache.ibatis.session.RowBounds;

/**
 *
 * @author teguhpa
 */
public interface CrudDAO<A extends EntityDTO, B extends SearchDTO> extends BaseDAO {
    public List<A> search(B searchDTO);
    public List<A> searchByPage(B searchDTO);
    public PagingResultDTO<A> search(B searchDTO, RowBounds rowBounds);
    public int delete(A entityDTO);
    public int update(A entityDTO);
    public int create(A entityDTO);
}
