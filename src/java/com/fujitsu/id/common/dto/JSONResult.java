/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.fujitsu.id.common.dto;

import com.fujitsu.id.sample.dto.MasterGateDTO;
import java.io.IOException;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import org.codehaus.jackson.map.ObjectMapper;

/**
 *
 * @author teguhpa
 */
public class JSONResult<T> {
    private T dto;
   
    public JSONResult(T dto) /*throws WebApplicationException*/ {
       this.dto = dto;
    }
    
    public String getJSONString() {
        String json = "";
        try {
             ObjectMapper objectMapper = new ObjectMapper();
            json = objectMapper.writeValueAsString(dto);
        } catch (IOException e) {
            e.printStackTrace();
           throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST)
                    .entity("Couldn't convert result to json: " + e.getMessage())
                    .build());
        }
        return json;
    }
   
    public static void main(String[] args) {
        MasterGateDTO dto = new MasterGateDTO();
        System.out.println(new JSONResult(dto).getJSONString());
    }

}
