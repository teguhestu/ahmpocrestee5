/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fujitsu.id.common.dto;

import com.fujitsu.id.sample.dto.EmployeeSearchDTO;
import java.io.IOException;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import org.codehaus.jackson.map.ObjectMapper;

/**
 *
 * @author teguhpa
 */
public class JSONParam<T extends BaseDTO> {

    private T dto;
    private String json;

    public JSONParam(String json) /*throws WebApplicationException*/ {
        this.json = json;
    }

    public T getDTO(Class<T> claz) {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
           // System.out.println(clazz.getSimpleName());
            //JavaType tp = TypeFactory.fromCanonical(dto.getClass().getName());
            dto = objectMapper.readValue(json, claz);
           //Genson genson = new Genson.Builder().setWithClassMetadata(true).create();
           //dto = genson.deserialize(json, claz);
        } catch (IOException e) {
            e.printStackTrace();
           throw new WebApplicationException(Response.status(Status.BAD_REQUEST)
                    .entity("Couldn't parse parameter string: " + e.getMessage())
                    .build());
        }
        return dto;
    }
    
    public static void main(String[] args) {
        String json = "{ \"empNo\":1, \"firstName\":\"teguh\", \"lastName\":\"pangestu\""
                + "}";
        JSONParam<EmployeeSearchDTO> js = new JSONParam<EmployeeSearchDTO>(json);
        EmployeeSearchDTO dto = js.getDTO(EmployeeSearchDTO.class);
        System.out.println(dto.getFirstName());
    }
}
