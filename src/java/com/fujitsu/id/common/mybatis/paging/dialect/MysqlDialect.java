/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fujitsu.id.common.mybatis.paging.dialect;

import com.fujitsu.id.common.mybatis.paging.Dialect;
import com.fujitsu.id.common.mybatis.paging.util.SqlRemoveHelper;

/**
 *
 * @author teguhpa
 */
public class MysqlDialect implements Dialect {

    @Override
    public String getLimitString(String sql, int offset, int limit) {
        return getLimitString(sql, offset, Integer.toString(offset),
                Integer.toString(limit));
    }

    @Override
    public boolean supportLimit() {
        return true;
    }

    public String getLimitString(String sql, int offset, String offsetPlaceholder, String limitPlaceholder) {
        StringBuilder stringBuilder = new StringBuilder(sql);
        stringBuilder.append(" limit ");
        if (offset > 0) {
            stringBuilder.append(offsetPlaceholder).append(",").append(limitPlaceholder);
        } else {
            stringBuilder.append(limitPlaceholder);
        }
        return stringBuilder.toString();
    }

    @Override
    public String getCountString(String querySqlString) {
        String sql = SqlRemoveHelper.removeOrders(querySqlString);
        return "select count(1) from (" + sql + ") as tmp_count";
    }
}
