/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.fujitsu.id.common.mybatis.paging;

/**
 *
 * @author teguhpa
 */
public class Page {
    private static final Integer DEFAULT_SIZE = 10;
    private Integer currentPage;
    private Integer rowsPerPage;
    private Integer totalRecords;
    private Integer totalPage;

    public Page(Integer currentPage, Integer rowsPerPage) {
        this.currentPage = currentPage;
        this.rowsPerPage = rowsPerPage;
    }

    public Integer getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(Integer currentPage) {
        this.currentPage = currentPage;
    }

    public Integer getRowsPerPage() {
        return rowsPerPage;
    }

    public void setRowsPerPage(Integer rowsPerPage) {
        this.rowsPerPage = rowsPerPage;
    }

    public Integer getTotalRecords() {
        return totalRecords;
    }

    public void setTotalRecords(Integer totalRecords) {
        this.totalRecords = totalRecords;
    }

    public Integer getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(Integer totalPage) {
        this.totalPage = totalPage;
    }
    
    
}
