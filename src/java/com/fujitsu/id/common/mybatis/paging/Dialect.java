/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.fujitsu.id.common.mybatis.paging;

/**
 *
 * @author teguhpa
 */
public interface Dialect {
    public boolean supportLimit();
    public String getLimitString(String sql, int offset, int limit);
    public String getCountString(String sql);
}
