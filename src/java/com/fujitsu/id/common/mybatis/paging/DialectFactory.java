/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.fujitsu.id.common.mybatis.paging;

import com.fujitsu.id.common.mybatis.paging.dialect.MSSQLDialect;
import com.fujitsu.id.common.mybatis.paging.dialect.MysqlDialect;
import com.fujitsu.id.common.mybatis.paging.dialect.OracleDialect;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author teguhpa
 */
public class DialectFactory {
    private static final long serialVersionUID = 8107330250767760951L;
	private static final Map<DBMS, Dialect> DBMS_DIALECT = new HashMap<DBMS, Dialect>();

	
	public static Dialect getDbmsDialect(DBMS dbms) {
		if (DBMS_DIALECT.containsKey(dbms)) {
			return DBMS_DIALECT.get(dbms);
		}
		Dialect dialect = createDbmsDialect(dbms);
		DBMS_DIALECT.put(dbms, dialect);
		return dialect;
        }

        public static void putEx(Dialect exDialect) {
		DBMS_DIALECT.put(DBMS.EX, exDialect);
	}
	
	private static Dialect createDbmsDialect(DBMS dbms) {
		switch (dbms) {
			case MYSQL:
				return new MysqlDialect();
			case ORACLE:
				return new OracleDialect();
			case SQLSERVER:
				return new MSSQLDialect();
			
			default:
				throw new UnsupportedOperationException("Empty dbms dialect");
		}
	}
}
