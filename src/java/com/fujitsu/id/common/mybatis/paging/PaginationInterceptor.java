/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fujitsu.id.common.mybatis.paging;

import com.fujitsu.id.common.dto.PagingResultDTO;
import com.fujitsu.id.common.utils.Reflections;
import com.google.common.base.Preconditions;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.Properties;
import org.apache.ibatis.executor.parameter.ParameterHandler;
import org.apache.ibatis.executor.resultset.ResultSetHandler;
import org.apache.ibatis.executor.statement.StatementHandler;
import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.plugin.Intercepts;
import org.apache.ibatis.plugin.Invocation;
import org.apache.ibatis.plugin.Plugin;
import org.apache.ibatis.plugin.Signature;
import org.apache.ibatis.reflection.MetaObject;
import org.apache.ibatis.reflection.SystemMetaObject;
import org.apache.ibatis.scripting.defaults.DefaultParameterHandler;
import org.apache.log4j.Logger;

/**
 *
 * @author teguhpa
 */
@Intercepts({
    @Signature(type = StatementHandler.class, method = "prepare", args = {Connection.class}),
    @Signature(type = ResultSetHandler.class, method = "handleResultSets", args = {Statement.class})})
public class PaginationInterceptor implements Interceptor {

    private static final Logger logger = Logger.getLogger(PaginationInterceptor.class);
    public static final ThreadLocal<PagingResultDTO> localPage = new ThreadLocal<PagingResultDTO>();
    public static final ThreadLocal<Boolean> usePaging = new ThreadLocal<Boolean>();
    protected Dialect dialect;
    protected String sqlRegex = "[*]";

  
    public static void startPage(int currentPage, int rowsPerPage) {
        localPage.set(new PagingResultDTO(currentPage, rowsPerPage));
        usePaging.set(Boolean.TRUE);
    }

   
    public static PagingResultDTO endPage() {
        PagingResultDTO page = localPage.get();
        localPage.remove();
        usePaging.remove();
        return page;
    }

    @Override
    public Object intercept(Invocation invocation) throws Throwable {
        if (localPage.get() == null) {
            return invocation.proceed();
        }
        if (invocation.getTarget() instanceof StatementHandler) {
            StatementHandler statementHandler = (StatementHandler) invocation.getTarget();
            MetaObject metaStatementHandler = SystemMetaObject.forObject(statementHandler);
            while (metaStatementHandler.hasGetter("h")) {
                Object object = metaStatementHandler.getValue("h");
                metaStatementHandler = SystemMetaObject.forObject(object);
            }
            
            while (metaStatementHandler.hasGetter("target")) {
                Object object = metaStatementHandler.getValue("target");
                metaStatementHandler = SystemMetaObject.forObject(object);
            }
            MappedStatement mappedStatement = (MappedStatement) metaStatementHandler.getValue("delegate.mappedStatement");
       
            if (mappedStatement.getId().matches(sqlRegex)) {
                usePaging.set(Boolean.TRUE);
                PagingResultDTO page = localPage.get();
                BoundSql boundSql = (BoundSql) metaStatementHandler.getValue("delegate.boundSql");

                String sql = boundSql.getSql();
                
                String pageSql = dialect.getLimitString(sql, page.getStartRow(), page.getRowsPerPage());

                metaStatementHandler.setValue("delegate.boundSql.sql", pageSql);
                Connection connection = (Connection) invocation.getArgs()[0];

                setPageParameter(sql, connection, mappedStatement, boundSql, page);
            }
            
            return invocation.proceed();
        } else if (usePaging.get() && invocation.getTarget() instanceof ResultSetHandler) {
            Object result = invocation.proceed();
            PagingResultDTO page = localPage.get();
            page.setDto((List) result);
            return result;
        }
        return null;
    }

    
    @Override
    public Object plugin(Object target) {
        if (target instanceof StatementHandler || target instanceof ResultSetHandler) {
            return Plugin.wrap(target, this);
        } else {
            return target;
        }
    }

    @Override
    public void setProperties(Properties p) {
        String dialectClass = p.getProperty("dialectClass");
        DBMS dbms;
        if (dialectClass == null || "".equals(dialectClass)) {
            String dialect = p.getProperty("dbms");
            Preconditions.checkArgument(!(dialect == null || "".equals(dialect)), "dialect property is not found!");
            dbms = DBMS.valueOf(dialect.toUpperCase());
            Preconditions.checkNotNull(dbms, "plugin not super on this database.");
        } else {
            Dialect dialect1 = (Dialect) Reflections.instance(dialectClass);
            Preconditions.checkNotNull(dialect1, "dialectClass is not found!");
            DialectFactory.putEx(dialect1);
            dbms = DBMS.EX;
        }

        dialect = DialectFactory.getDbmsDialect(dbms);

        String sql_regex = p.getProperty("sqlPattern");
        if (!(sql_regex == null || "".equals(sql_regex))) {
            sqlRegex = sql_regex;
        }
       
    }

    
    private void setPageParameter(String sql, Connection connection, MappedStatement mappedStatement,
            BoundSql boundSql, PagingResultDTO page) {
       
        String countSql = dialect.getCountString(sql);
        PreparedStatement countStmt = null;
        ResultSet rs = null;
        try {
            countStmt = connection.prepareStatement(countSql);
            BoundSql countBS = new BoundSql(mappedStatement.getConfiguration(), countSql,
                    boundSql.getParameterMappings(), boundSql.getParameterObject());
            setParameters(countStmt, mappedStatement, countBS, boundSql.getParameterObject());
            rs = countStmt.executeQuery();
            int totalCount = 0;
            if (rs.next()) {
                totalCount = rs.getInt(1);
            }
            page.setTotalRecords(totalCount);
            int totalPage = totalCount / page.getRowsPerPage() + ((totalCount % page.getRowsPerPage() == 0) ? 0 : 1);
            page.setTotalPage(totalPage);
        } catch (SQLException e) {
            logger.error("Ignore this exception", e);
        } finally {
            try {
                rs.close();
            } catch (SQLException e) {
                logger.error("Ignore this exception", e);
            }
            try {
                countStmt.close();
            } catch (SQLException e) {
                logger.error("Ignore this exception", e);
            }
        }
    }

    
    private void setParameters(PreparedStatement ps, MappedStatement mappedStatement, BoundSql boundSql,
            Object parameterObject) throws SQLException {
        ParameterHandler parameterHandler = new DefaultParameterHandler(mappedStatement, parameterObject, boundSql);
        parameterHandler.setParameters(ps);
    }
}
