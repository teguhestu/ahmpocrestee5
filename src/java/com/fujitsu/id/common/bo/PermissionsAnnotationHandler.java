/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fujitsu.id.common.bo;

import com.fujitsu.id.common.auth.*;
import com.fujitsu.id.common.exception.NotAuthorizedException;
import com.fujitsu.id.sample.bo.AuthBO;
import java.lang.annotation.Annotation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author teguh.estu
 */
@Service
public class PermissionsAnnotationHandler extends AuthAnnotationHandler {

    @Autowired
    private AuthBO authBO;

    protected String[] getAnnotationValue(Annotation a) {
        RequiresPermissions rpAnnotation = (RequiresPermissions) a;
        return rpAnnotation.value();
    }

    public void assertAuthorized(String token, Annotation a)
            throws NotAuthorizedException {
        System.out.println(authBO == null);
        String[] permission = getAnnotationValue(a);
        if (permission.length > 0) {
            //if (((a instanceof RequiresUser))) {
            if (!authBO.isPermissionAllowed(token, permission[0])) {
                throw new NotAuthorizedException("Attempting to perform a user-only operation.  The current Subject is not a user (they haven't been authenticated or remembered from a previous login).  Access denied.");
            }
        }
    }
}
