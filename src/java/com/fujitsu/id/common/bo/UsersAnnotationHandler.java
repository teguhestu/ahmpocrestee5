/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fujitsu.id.common.bo;

import com.fujitsu.id.sample.bo.AuthBO;
import com.fujitsu.id.common.exception.NotAuthorizedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author teguh.estu
 */
@Component
public class UsersAnnotationHandler extends AuthAnnotationHandler {
    
    @Autowired
    private AuthBO authBO;
    
    public void assertAuthorized(String token)
  {
      //AuthBO authBO = new AuthBOImpl();
      System.out.println(authBO == null);
    //if (((a instanceof RequiresUser))) {
      if (!authBO.isAuthenticated(token)) {  
        throw new NotAuthorizedException("Attempting to perform a user-only operation.  The current Subject is not a user (they haven't been authenticated or remembered from a previous login).  Access denied.");
      }
    //}
  }
}
