/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.fujitsu.id.common.bo;

import com.fujitsu.id.common.dao.CrudDAO;
import com.fujitsu.id.common.dto.EntityDTO;
import com.fujitsu.id.common.dto.PagingResultDTO;
import com.fujitsu.id.common.dto.SearchDTO;
import com.fujitsu.id.common.dto.validation.group.CreateGroup;
import com.fujitsu.id.common.dto.validation.group.DeleteGroup;
import com.fujitsu.id.common.dto.validation.group.UpdateGroup;
import com.fujitsu.id.common.mybatis.paging.PaginationInterceptor;
import java.util.List;
import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import org.springframework.stereotype.Service;

/**
 *
 * @author teguhpa
 */
@Service
public abstract class CrudBO<A extends EntityDTO, B extends SearchDTO> {
                
    public PagingResultDTO<A> searchByPage(B searchDTO, int currentPage, int rowsPerPage) {
        PaginationInterceptor.startPage(currentPage, rowsPerPage);
        getDAO().searchByPage(searchDTO);
       return  PaginationInterceptor.endPage();
    }
    
    public List<A> search(B searchDTO) {
       List<A> searchResult = getDAO().search(searchDTO);
       return  searchResult;
    }
    
    public void create (A entityDTO) {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Set<ConstraintViolation<A>> errors = validator.validate(entityDTO, CreateGroup.class);
        if (errors.size() > 0) {
            throw new ConstraintViolationException(errors);
        }
        getDAO().create(entityDTO);
    }
    
    public void update (A entityDTO) {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Set<ConstraintViolation<A>> errors = validator.validate(entityDTO, UpdateGroup.class);
        if (errors.size() > 0) {
            throw new ConstraintViolationException(errors);
        }
        getDAO().update(entityDTO);
    }
    
    public void delete (A entityDTO) {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Set<ConstraintViolation<A>> errors = validator.validate(entityDTO, DeleteGroup.class);
        if (errors.size() > 0) {
            throw new ConstraintViolationException(errors);
        }
        getDAO().delete(entityDTO);
    }
    
    public void deleteMultiple(List<A> entityDTOs) {
        for (A dto: entityDTOs) {
            delete(dto);
        }
    }
    
    public abstract CrudDAO getDAO();
    
}
